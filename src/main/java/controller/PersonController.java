package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Person;

public class PersonController {
    
    private final Person person;
    
    @FXML
    private Label lblGreeting;
    
    @FXML
    private Button btnShow;
    
    @FXML
    private TextField txtEjemplo;
    
    @FXML
    private Button btnLimpiar;
    
    public PersonController(Person person) {
        this.person = person;
    }
    
    public void initialize(){
        lblGreeting.setText("");
        btnShow.setOnAction((actionEvent) -> {
            var salutation = this.person.getSalutation();
            lblGreeting.setText(salutation);
        });
              
        this.btnLimpiar.setOnAction((t) -> {
            this.txtEjemplo.clear();
        });
        this.btnLimpiar.disableProperty().bind(this.txtEjemplo.textProperty().isEmpty());
    }
}
