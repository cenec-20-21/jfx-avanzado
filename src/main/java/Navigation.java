import controller.INavigation;
import controller.ScreenEnum;
import java.util.HashMap;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Navigation implements INavigation {

    private final HashMap<ScreenEnum, Scene> scenes;
    private final Stage stage;
    
    public Navigation(Stage stage){
        this.scenes = new HashMap<>();
        this.stage = stage;
    }
    
    public void Add(ScreenEnum screenEnum, Scene scene){
        this.scenes.put(screenEnum, scene);
    }
    
    @Override
    public void Navigate(ScreenEnum screenEnum) {
        var scene = this.scenes.get(screenEnum);
        this.stage.setScene(scene);
        this.stage.show();
    }
}
