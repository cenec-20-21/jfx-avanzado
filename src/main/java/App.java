import controller.INavigation;
import controller.LoginController;
import controller.PersonController;
import controller.ScreenEnum;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.InMemoryCredentialsRepository;
import model.Login;
import model.Person;
import model.SqliteCredentialsRepository;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Ejemplo avanzado de Java FX");
            stage.setOnCloseRequest(windowEvent -> {
                exitApplication(windowEvent);
            });
        try {
            var navigation = wireScreens(stage);
            navigation.Navigate(ScreenEnum.Login);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private INavigation wireScreens(Stage stage) throws IOException {
        var navigation = new Navigation(stage);
        
        var loginLoader = new FXMLLoader();
        loginLoader.setLocation(getClass().getResource("/view/LoginView.fxml"));
        //var credentialsRepository = new InMemoryCredentialsRepository();
        var credentialsRepository = new SqliteCredentialsRepository();
        var login = new Login(credentialsRepository);
        var loginController = new LoginController(navigation, login);
        loginLoader.setController(loginController);
        var loginRoot = (Pane) loginLoader.load();
        var loginScene = new Scene(loginRoot);
        loginScene.getStylesheets().add(getClass().getResource("style/app.css").toExternalForm());
        
        var personLoader = new FXMLLoader();
        personLoader.setLocation(getClass().getResource("/view/PersonView.fxml"));
        var person = new Person();
        var personController = new PersonController(person);
        personLoader.setController(personController);
        var personRoot = (Pane) personLoader.load();
        var personScene = new Scene(personRoot);
        personScene.getStylesheets().add(getClass().getResource("style/app.css").toExternalForm());
        
        navigation.Add(ScreenEnum.Login, loginScene);
        navigation.Add(ScreenEnum.Main, personScene);
        
        return navigation;
    }
    
    public static void main(String[] args) {
        launch();
    }
    
    private void exitApplication(Event e){
        var alert = new Alert(Alert.AlertType.CONFIRMATION, "¿Estás seguro de que deseas salir?");
            var result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK){
                Platform.exit();
            } else {
                e.consume();
            }
    }
}