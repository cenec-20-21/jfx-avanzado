package model;

public class Person {
    
    private String salutation;
    
    public Person(){
        this.salutation = "Hola, soy una persona de ejemplo";
    }
    
    public String getSalutation(){
       return this.salutation;
    }
    
    public void setSalutation(String salutation){
        if(salutation != null){
            this.salutation = salutation;
        }
    }
}
