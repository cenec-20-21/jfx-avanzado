# jfx-avanzado

Advanced Java FX

## IntelliJ

### Create JavaFX project with Maven
1. Create project from Maven archetype for Java FX. If it does not exist, add a new archetype with GroupIdd `org.openjfx`, ArtifactId `javafx-maven-archetypes` and Version `0.0.5`
2. Create new project as shown in the image
   ![new project snapshot](intelliJ01.PNG)
3. Edit properties for Maven as follows
    ```
    archetypeArtifactId: javafx-archetype-fxml
    javafx-version: 15.0.11
   ```
   ![maven properties snapshot](intelliJ02.PNG)

More info at https://openjfx.io/openjfx-docs

### Run JavaFx project
1. Open the Maven Projects window at View > Tool Windows > Maven and double click on compiler > `compiler:compiler` to compile and in javafx > `javafx:run` to launch
   ![maven snapshot](intelliJ03.PNG)
   NOTE: If `JAVA_HOME` is not set to 11 or greater, got to Settings > Build, Execution, Deployment > Maven > Runner and add the environment variable
   `JAVA_HOME="C:\Program Files\AdoptOpenJDK\jdk-11.0.10.9-hotspot"` pointing to the appropriate JDK
   ![maven environment variable](intelliJ04.PNG)
   

### To use database SqlLite
1. Download DB Browser for SQL lite. Execute application, it'll create an empty database but it will fail because there is no User table
2. Create a table User
   ```
   CREATE TABLE "User" (
       "id"	INTEGER,
       "username"	TEXT NOT NULL,
       "password"	TEXT NOT NULL,
       PRIMARY KEY("id"));
   ```
3. Insert credentials
   ```
   INSERT into User VALUES (1, "admin", "admin");
   ```
4. Update credentials
   ```
   UPDATE User
   SET username = 'cenec', password = 'cenec'
   WHERE id = 1;
   ```